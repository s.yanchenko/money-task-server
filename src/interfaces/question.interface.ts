export interface Question {
  taskId: string
  questions: {
    title: string
    rightOption: string
    variants: { title: string }[]
  }[]
}

export interface Answer {
  questionId: string
  answerOption: string
}

export type QuestionUpdateData = Partial<Record<string, unknown>>

export type QuestionUpdateFilter = Partial<Record<string, unknown>>
