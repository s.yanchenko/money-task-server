interface CustomError {
  statusCode: number
  message: string
}

export default CustomError
