export interface User {
  nickname: string
  password: string
  avatarUrl?: string
  roles: string[]
  currentBalance: number
  totalBalance: number
  availableTasksTodayCount: number
  availableMoneyForWithdrawal: number
  chatId?: string
}

export interface AccessTokenData {
  id: string
  nickname: string
  roles: string[]
}

export type UserUpdateProps = Partial<Record<string, unknown>>
