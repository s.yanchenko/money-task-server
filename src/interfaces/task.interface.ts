import { Types } from 'mongoose'

export interface Task {
  shortDescription: string
  description?: string | null
  status: string
  type: string
  userId: string
  taskDisplayedDate?: string | null
  dates: {
    start: string
    end?: string | null
  }[]
  relatedTaskId?: string | null
  remunerationAmount: number
}

export type TaskWithId = Task & { _id: Types.ObjectId }

export type TaskUpdateData = Partial<Record<string, unknown>>

export type TaskUpdateFilter = Partial<Record<string, unknown>>

export type TaskCreateProps = Omit<
  Task,
  'userId' | 'taskDisplayedDate' | 'dates' | 'relatedTaskId'
>
