import HttpException from '@/utils/exceptions/http.exception'
import HttpCode from '@/constants/http.code.constant'
import Message from '@/constants/message.constant'
import {
  TaskCreateProps,
  TaskUpdateData,
  TaskUpdateFilter,
  TaskWithId,
} from '@/interfaces/task.interface'
import TaskModel from '@/models/task.model'

export const TaskService = {
  async create(data: TaskCreateProps, userId: string) {
    await TaskModel.create({
      shortDescription: data.shortDescription,
      description: data?.description,
      status: data.status,
      type: data.type,
      userId,
      remunerationAmount: data.remunerationAmount,
    })
  },
  async delete(id: string) {
    await TaskModel.deleteOne({ _id: id })
  },
  async edit(
    id: string,
    data: TaskUpdateData,
    filter: TaskUpdateFilter | undefined = {},
  ) {
    if (!id || !data) {
      throw new HttpException(HttpCode.BAD_REQUEST, Message.WRONG_DATA)
    }

    await TaskModel.updateOne({ _id: id, ...filter }, { ...data })
  },
  async getById({ taskId, userId }: { taskId?: string; userId?: string }) {
    const task = await TaskModel.findOne({
      $or: [{ _id: taskId }, { userId: userId }],
    })

    if (!task) {
      throw new HttpException(HttpCode.NOT_FOUND, Message.MISSING_DATA)
    }

    return task
  },
  async getByFilter(filter: Record<string, unknown>): Promise<TaskWithId[]> {
    const task = await TaskModel.find(filter)

    if (!task) {
      throw new HttpException(HttpCode.NOT_FOUND, Message.MISSING_DATA)
    }

    return task
  },
}
