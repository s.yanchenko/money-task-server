import TelegramBot from '@/utils/telegram-bot/initiate'
import { UserService } from '@/services/user.service'

export const TelegramBotService = {
  async registration() {
    TelegramBot.bot.on('message', async (message) => {
      if (message.text === '/start') {
        await TelegramBot.bot.sendMessage(message.chat.id, 'Введи свой никнейм')
      }

      const user = await UserService.get({ nickname: message.text })

      if (!user) {
        return TelegramBot.bot.sendMessage(
          message.chat.id,
          'Мы не нашли такого пользователя',
        )
      }

      await UserService.edit(String(user._id), { chatId: message.chat.id })

      return TelegramBot.bot.sendMessage(
        message.chat.id,
        'Мы не нашли такого пользователя',
      )
    })
  },
  async send(chatId: string, message: string) {
    await TelegramBot.bot.sendMessage(chatId, message)
  },
}
