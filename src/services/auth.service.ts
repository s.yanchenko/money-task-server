import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'
import HttpException from '@/utils/exceptions/http.exception'
import HttpCode from '@/constants/http.code.constant'
import Message from '@/constants/message.constant'
import Api from '@/constants/api.constant'

export const AuthService = {
  async checkPassword(password: string, userPassword: string) {
    const isPassEquals = await bcrypt.compare(password, userPassword)

    if (!isPassEquals) {
      throw new HttpException(HttpCode.BAD_REQUEST, Message.WRONG_DATA)
    }

    return isPassEquals
  },
  async generateAccessToken(id: string, nickname: string, roles: string[]) {
    return jwt.sign(
      { id, nickname, roles },
      process.env.JWT_ACCESS_SECRET || Api.JWT_ACCESS_SECRET,
    )
  },
  validateAccessToken(accessToken: string) {
    const verify = jwt.verify(
      accessToken,
      process.env.JWT_ACCESS_SECRET || Api.JWT_ACCESS_SECRET,
    )
    return Boolean(verify)
  },
  getAccessTokenData(accessToken: string) {
    return jwt.verify(
      accessToken,
      process.env.JWT_ACCESS_SECRET || Api.JWT_ACCESS_SECRET,
    )
  },
}
