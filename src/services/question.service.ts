import HttpException from '@/utils/exceptions/http.exception'
import HttpCode from '@/constants/http.code.constant'
import Message from '@/constants/message.constant'
import QuestionModel from '@/models/question.model'
import {
  Question,
  QuestionUpdateData,
  QuestionUpdateFilter,
} from '@/interfaces/question.interface'

export const QuestionService = {
  async create(data: Question) {
    await QuestionModel.create({ ...data })
  },
  async delete(id: string) {
    await QuestionModel.deleteOne({ _id: id })
  },
  async edit(
    id: string,
    data: QuestionUpdateData,
    filter: QuestionUpdateFilter | undefined = {},
  ) {
    if (!id || !data) {
      throw new HttpException(HttpCode.BAD_REQUEST, Message.WRONG_DATA)
    }

    await QuestionModel.updateOne({ _id: id, ...filter }, { ...data })
  },
  async getById({
    questionId,
    taskId,
  }: {
    questionId?: string
    taskId?: string
  }) {
    const questions = await QuestionModel.findOne({
      $or: [{ _id: questionId }, { taskId }],
    })

    if (!questions) {
      throw new HttpException(HttpCode.NOT_FOUND, Message.MISSING_DATA)
    }

    return questions
  },
}
