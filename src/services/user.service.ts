import bcrypt from 'bcrypt'
import UserModel from '@/models/user.model'
import { User, UserUpdateProps } from '@/interfaces/user.interface'
import Message from '@/constants/message.constant'
import HttpException from '@/utils/exceptions/http.exception'
import HttpCode from '@/constants/http.code.constant'
import UserConstant from '@/constants/user.constant'

export const UserService = {
  async create(data: Pick<User, 'nickname' | 'password'>) {
    const candidate = await UserModel.findOne({ nickname: data.nickname })

    if (candidate) {
      throw new HttpException(HttpCode.BAD_REQUEST, Message.USER_ALREADY_EXISTS)
    }

    const hashPassword = await bcrypt.hash(data.password, 7)

    await UserModel.create({
      nickname: data.nickname,
      password: hashPassword,
      roles: [UserConstant.DEFAULT_ROLE],
      currentBalance: 0,
      totalBalance: 0,
      availableTasksTodayCount: UserConstant.DEFAULT_AVAILABLE_TASKS_COUNT,
      availableMoneyForWithdrawal: UserConstant.MAX_AVAILABLE_MONEY_WITHDRAWAL,
    })
  },
  async delete(id: string) {
    await UserModel.deleteOne({ _id: id })
  },
  async edit(id: string, data: UserUpdateProps) {
    await UserModel.updateOne({ _id: id }, { ...data })
  },
  async get(data: { nickname?: string; id?: string; roles?: string[] }) {
    const candidate = await UserModel.findOne({
      $or: [
        {
          nickname: data?.nickname,
        },
        {
          _id: data?.id,
        },
        {
          roles: { $in: data?.roles },
        },
      ],
    })

    if (!candidate) {
      throw new HttpException(HttpCode.NOT_FOUND, Message.MISSING_DATA)
    }

    return candidate
  },
}
