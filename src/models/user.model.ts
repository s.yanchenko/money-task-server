import { model, Schema } from 'mongoose'

const UserModel = model(
  'user',
  new Schema({
    nickname: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    avatarUrl: { type: String, required: false },
    roles: { type: [String], required: true },
    currentBalance: { type: Number, required: true },
    totalBalance: { type: Number, required: true },
    availableTasksTodayCount: { type: Number, required: true },
    availableMoneyForWithdrawal: { type: Number, required: true },
    chatId: { type: String, required: false },
  }),
)

export default UserModel
