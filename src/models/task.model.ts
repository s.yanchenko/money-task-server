import { model, Schema } from 'mongoose'

const TaskModel = model(
  'task',
  new Schema({
    shortDescription: { type: String, required: true },
    description: { type: String, required: false },
    status: { type: String, required: true },
    type: { type: String, required: true },
    userId: { type: String, required: true },
    taskDisplayedDate: { type: String, required: false },
    dates: [
      {
        start: { type: String, required: true },
        end: { type: String, required: false },
      },
    ],
    relatedTaskId: { type: String, required: false },
    remunerationAmount: { type: Number, required: true },
  }),
)

export default TaskModel
