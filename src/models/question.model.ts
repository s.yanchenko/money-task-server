import { model, Schema } from 'mongoose'

const QuestionItemSchema = new Schema({
  title: { type: String, required: true },
  variants: [
    {
      title: { type: String, required: true },
    },
  ],
  rightOption: { type: String, required: true },
})

const QuestionModel = model(
  'question',
  new Schema({
    taskId: { type: String, required: true },
    questions: [QuestionItemSchema],
  }),
)

export default QuestionModel
