import express, { Express } from 'express'
import dotenv from 'dotenv'
import authRouter from '@/routers/auth.router'
import userRouter from '@/routers/user.router'
import taskRouter from '@/routers/task.router'
import questionsRouter from '@/routers/question.router'
import Api from '@/constants/api.constant'
import mongoose from 'mongoose'
import cookieParser from 'cookie-parser'

dotenv.config()

const app: Express = express()
const port = Api.PORT
const mongoUri = Api.MONGO_URI

app.use(cookieParser())

app.use(Api.AUTH, express.json(), authRouter)
app.use(Api.USER, express.json(), userRouter)
app.use(Api.TASK, express.json(), taskRouter)
app.use(Api.QUESTIONS, express.json(), questionsRouter)
;(async () => {
  await mongoose.connect(mongoUri)
  app.listen(port, () => console.log(`Server started on PORT = ${port}`))
})()
