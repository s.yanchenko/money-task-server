import express from 'express'
import { AuthController } from '@/controllers/auth.controller'
import Api from '@/constants/api.constant'

const router = express.Router()

router.post(Api.AUTH_REGISTRATION, AuthController.registration)
router.post(Api.AUTH_LOGIN, AuthController.login)
router.post(Api.AUTH_LOGOUT, AuthController.logout)

export default router
