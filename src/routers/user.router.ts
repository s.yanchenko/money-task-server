import express from 'express'
import { UserController } from '@/controllers/user.controller'
import { AuthMiddleware } from '@/middlewares/auth.middleware'
import { MulterStorage } from '@/utils/storage/multer.storage'
import Api from '@/constants/api.constant'

const upload = MulterStorage()

const router = express.Router()

router.get(Api.USER_GET_PROFILE, AuthMiddleware, UserController.getProfile)
router.patch(Api.USER_PATCH_PROFILE, AuthMiddleware, UserController.editProfile)
router.post(
  Api.USER_POST_PROFILE_AVATAR,
  upload.single('avatar'),
  AuthMiddleware,
  UserController.editProfileAvatar,
)
router.get(
  Api.USER_GET_PROFILE_AVATAR,
  AuthMiddleware,
  UserController.getProfileAvatar,
)
router.get(
  Api.USER_GET_BALANCE_DATA,
  AuthMiddleware,
  UserController.getBalanceData,
)
router.post(
  Api.USER_REQUEST_WITHDRAWAL,
  AuthMiddleware,
  UserController.requestWithdrawal,
)

export default router
