import express from 'express'
import { QuestionController } from '@/controllers/question.controller'
import Api from '@/constants/api.constant'

const router = express.Router()

router.get(Api.QUESTION_GET_FOR_TASK, QuestionController.getQuestionsByTaskId)
router.post(
  Api.QUESTION_CHECK_BY_TASK_ID,
  QuestionController.checkQuestionsByTaskId,
)

export default router
