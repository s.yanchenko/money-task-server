import express from 'express'
import Api from '@/constants/api.constant'
import { AuthMiddleware } from '@/middlewares/auth.middleware'
import { TaskController } from '@/controllers/task.controller'
import { AdminMiddleware } from '@/middlewares/admin.middleware'

const router = express.Router()

router.get(Api.TASK_GET_TASK_BY_ID, AuthMiddleware, TaskController.getTaskById)
router.get(
  Api.TASK_GET_EXECUTION_TASKS,
  AuthMiddleware,
  TaskController.getExecutionTasks,
)
router.get(
  Api.TASK_GET_AVAILABLE_TASKS,
  AuthMiddleware,
  TaskController.getAvailableTasks,
)
router.patch(
  Api.TASK_START_TASK_BY_ID,
  AuthMiddleware,
  TaskController.startTaskById,
)
router.patch(
  Api.TASK_TOGGLE_TASK_TRACKER_STATUS_BY_ID,
  AuthMiddleware,
  TaskController.toggleTaskTrackerStatusById,
)
router.post(
  Api.TASK_CREATE,
  AuthMiddleware,
  AdminMiddleware,
  TaskController.createTask,
)
router.post(
  Api.TASK_CREATE_PROPOSED,
  AuthMiddleware,
  TaskController.createProposedTask,
)
router.patch(Api.TASK_FINISH, AuthMiddleware, TaskController.finishTaskById)

export default router
