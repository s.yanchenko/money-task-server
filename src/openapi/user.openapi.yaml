openapi: 3.0.0
info:
  description: 'Swagger user routes'
  version: "1.0.0"
  title: 'User'
servers:
  -   description: Local
      url: http://localhost:5000/api/user
  -   description: Prod
      url: https://money-task/api/user
tags:
  - name: User
    description: 'Пользователь'
paths:
  /profile:
    get:
      tags:
        - User
      summary: Получение данных пользователя
      operationId: getProfile
      responses:
        '200':
          description: "Успешное выполнение запроса"
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/GetProfileModel'
        '400':
          $ref: "#/components/schemas/ApiError"
        '401':
          $ref: "#/components/schemas/ApiError"
        '500':
          $ref: "#/components/schemas/ApiError"
    patch:
      tags:
        - User
      summary: Изменение данных пользователя
      operationId: editProfile
      requestBody:
        content:
          application/json:
            schema:
              type: object
              required:
                - nickname
              properties:
                nickname:
                  type: string
                  description: Никнейм пользователя
                  example: "UserNickname"
      responses:
        '200':
          description: "Успешное выполнение запроса"
        '400':
          $ref: "#/components/schemas/ApiError"
        '401':
          $ref: "#/components/schemas/ApiError"
        '500':
          $ref: "#/components/schemas/ApiError"

  /profile-avatar:
    post:
      tags:
        - User
      summary: Редактирование аватара пользователя
      operationId: editProfileAvatar
      requestBody:
        content:
          multipart/form-data:
            schema:
              type: object
              required:
                - avatar
              properties:
                avatar:
                  type: string
                  format: binary
      responses:
        '200':
          description: "Успешное выполнение запроса"
        '400':
          $ref: "#/components/schemas/ApiError"
        '401':
          $ref: "#/components/schemas/ApiError"
        '500':
          $ref: "#/components/schemas/ApiError"

  /get-balance-data:
    get:
      tags:
        - User
      summary: Получить данные баланса пользователя
      operationId: getBalanceData
      responses:
        '200':
          description: "Успешное выполнение запроса"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/GetProfileBalanceModel"
        '400':
          $ref: "#/components/schemas/ApiError"
        '401':
          $ref: "#/components/schemas/ApiError"
        '500':
          $ref: "#/components/schemas/ApiError"

  /request-withdrawal:
    post:
      tags:
        - User
      summary: Запросить вывод средств
      operationId: requestWithdrawal
      requestBody:
        content:
          application/json:
            schema:
              type: object
              required:
                - amount
              properties:
                amount:
                  type: number
                  description: Сумма для вывода
                  example: 1000
      responses:
        '200':
          description: "Успешное выполнение запроса"
        '400':
          $ref: "#/components/schemas/ApiError"
        '401':
          $ref: "#/components/schemas/ApiError"
        '500':
          $ref: "#/components/schemas/ApiError"

components:
  securitySchemes:
    accessToken:
      type: apiKey
      in: cookie
      name: accessToken
  schemas:
    ApiError:
      type: object
      description: Объект ошибки
      properties:
        status:
          type: number
          description: Статус ошибки
          example: 400
        message:
          type: string
          description: Сообщение ошибки
          example: "Error message"
    GetProfileModel:
      type: object
      required:
        - nickname
        - currentBalance
        - totalBalance
      properties:
        nickname:
          type: string
          description: Никнейм пользователя
          example: "UserNickname"
        avatarUrl:
          type: string
          format: url
          description: URL аватара пользователя
          example: 'http://localhost:5000/api/user/:filename'
        currentBalance:
          type: number
          description: Текущий баланс
          example: 1000
        totalBalance:
          type: number
          description: Общий баланс
          example: 3000
    GetProfileBalanceModel:
      type: object
      required:
        - currentBalance
        - availableMoneyForWithdrawal
      properties:
        currentBalance:
          type: number
          description: Текущий баланс
          example: 1500
        availableMoneyForWithdrawal:
          type: number
          description: Разрешенная для вывода сумма
          example: 1500