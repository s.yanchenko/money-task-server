import { NextFunction, Request, Response } from 'express'
import HttpException, { sendApiError } from '@/utils/exceptions/http.exception'
import HttpCode from '@/constants/http.code.constant'
import Message from '@/constants/message.constant'
import { AuthService } from '@/services/auth.service'
import { AccessTokenData } from '@/interfaces/user.interface'
import UserConstant from '@/constants/user.constant'

export const AdminMiddleware = async (
  req: Request,
  res: Response,
  next: NextFunction,
) => {
  try {
    const { accessToken } = req.cookies

    if (!accessToken) {
      throw new HttpException(HttpCode.UNAUTHORIZED, Message.UNAUTHORIZED)
    }

    const tokenData = AuthService.getAccessTokenData(accessToken)

    if (
      !(tokenData as AccessTokenData).roles.includes(UserConstant.ADMIN_ROLE)
    ) {
      throw new HttpException(HttpCode.FORBIDDEN, Message.FORBIDDEN)
    }

    next()
  } catch (error: unknown) {
    sendApiError(error, res)
  }
}
