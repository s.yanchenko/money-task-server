import { NextFunction, Request, Response } from 'express'
import HttpException, { sendApiError } from '@/utils/exceptions/http.exception'
import HttpCode from '@/constants/http.code.constant'
import Message from '@/constants/message.constant'
import { AuthService } from '@/services/auth.service'

export const AuthMiddleware = async (
  req: Request,
  res: Response,
  next: NextFunction,
) => {
  try {
    const { accessToken } = req.cookies

    if (!accessToken) {
      throw new HttpException(HttpCode.UNAUTHORIZED, Message.UNAUTHORIZED)
    }

    const isAccessTokenValid = AuthService.validateAccessToken(accessToken)

    if (!isAccessTokenValid) {
      throw new HttpException(HttpCode.UNAUTHORIZED, Message.UNAUTHORIZED)
    }

    next()
  } catch (error: unknown) {
    sendApiError(error, res)
  }
}
