const Message = {
  SOMETHING_WENT_WRONG: 'Что-то пошло не так',
  INVALID_TYPE_ERROR: 'Неверный тип поля',
  MISSING_DATA: 'Отсутствуют данные',
  WRONG_DATA: 'Неверные данные',
  REQUIRED_ERROR: 'Обязательное поле',
  USER_NOT_FOUND: 'Пользователь не найден',
  USER_ALREADY_EXISTS: 'Пользователь уже существует',
  UNAUTHORIZED: 'Пользователь не авторизован',
  FORBIDDEN: 'Нет доступа',
  INVALID_WITHDRAWAL: 'Превышена максимальная доступная сумма для вывода',
}
export default Message
