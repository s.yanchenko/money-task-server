const UserConstant = {
  ADMIN_ROLE: 'admin',
  DEFAULT_ROLE: 'default',
  DEFAULT_AVAILABLE_TASKS_COUNT: 5,
  MAX_AVAILABLE_MONEY_WITHDRAWAL: 15000,
}

export default UserConstant
