export const TaskType = {
  BOOK: 'book',
  FITNESS: 'fitness',
  FILM: 'film',
  TELEGRAM: 'telegram-bot',
  YOUTUBE: 'youtube',
  OTHER: 'other',
}

export const TaskStatus = {
  AVAILABLE: 'available',
  SUSPENDED: 'suspended',
  PROGRESS: 'progress',
  COMPLETED: 'completed',
  PROPOSED: 'proposed',
  OVERDUE: 'overdue',
  DISPLAYED: 'displayed',
}
