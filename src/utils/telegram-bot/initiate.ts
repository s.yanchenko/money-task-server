import dotenv from 'dotenv'
import TelegramApi from 'node-telegram-bot-api'

dotenv.config()

const token = process.env.TELEGRAM_API_TOKEN || ''

class TelegramBot {
  public bot

  constructor() {
    this.bot = new TelegramApi(token, { polling: true })
  }
}

export default new TelegramBot()
