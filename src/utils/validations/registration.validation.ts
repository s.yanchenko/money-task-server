import { z } from 'zod'
import Message from '@/constants/message.constant'

export const registrationValidation = z.object({
  nickname: z
    .string({
      invalid_type_error: Message.INVALID_TYPE_ERROR,
      required_error: Message.REQUIRED_ERROR,
    })
    .min(4, 'Минимальное количество символов - 4'),
  password: z
    .string({
      invalid_type_error: Message.INVALID_TYPE_ERROR,
      required_error: Message.REQUIRED_ERROR,
    })
    .min(8, 'Минимальное количество символов - 8'),
})
