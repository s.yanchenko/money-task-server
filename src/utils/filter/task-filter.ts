import { Task, TaskWithId } from '@/interfaces/task.interface'
import { TaskStatus } from '@/constants/task.constant'

export const taskFilter = (task: Task) => ({
  shortDescription: task.shortDescription,
  description: task.description,
  status: task.status,
  type: task.type,
  dates: task.dates.map((el) => ({ start: el.start, end: el.end })),
  remunerationAmount: task.remunerationAmount,
})

export const setTaskDisplayedStatus = (task: TaskWithId) => ({
  _id: task._id,
  shortDescription: task.shortDescription,
  description: task.description,
  status: TaskStatus.DISPLAYED,
  type: task.type,
  userId: task.userId,
  taskDisplayedDate: task.taskDisplayedDate,
  dates: task.dates.map((el) => ({ start: el.start, end: el.end })),
  relatedTaskId: task.relatedTaskId,
  remunerationAmount: task.remunerationAmount,
})
