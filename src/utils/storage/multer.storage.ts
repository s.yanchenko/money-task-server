import crypto from 'crypto'
import path from 'path'
import { GridFsStorage } from 'multer-gridfs-storage'
import dotenv from 'dotenv'
import Api from '@/constants/api.constant'
import multer from 'multer'

dotenv.config()

export const MulterStorage = () => {
  const storage = new GridFsStorage({
    url: process.env.MONGO_URI || Api.MONGO_URI,
    file: (req, file) => {
      return new Promise((resolve, reject) => {
        crypto.randomBytes(16, (err, buf) => {
          if (err) {
            return reject(err)
          }
          const filename = buf.toString('hex') + path.extname(file.originalname)
          const fileInfo = {
            filename,
            bucketName: Api.BUCKET_NAME,
          }
          resolve(fileInfo)
        })
      })
    },
  })

  return multer({
    storage,
  })
}
