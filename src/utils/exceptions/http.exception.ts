import CustomError from '@/interfaces/error.interface'
import HttpCode from '@/constants/http.code.constant'
import Message from '@/constants/message.constant'
import { Response } from 'express'

class HttpException extends Error {
  public statusCode: number
  public msg: string
  constructor(statusCode: number, msg: string) {
    super(msg)
    this.statusCode = statusCode
    this.msg = msg
  }
}

export const sendApiError = (error: unknown, res: Response) => {
  const errData = new HttpException(
    (error as CustomError)?.statusCode || HttpCode.INTERNAL_SERVER_ERROR,
    (error as CustomError)?.message || Message.SOMETHING_WENT_WRONG,
  )
  return res
    .status(errData.statusCode)
    .send({ status: errData.statusCode, message: errData.msg })
}

export default HttpException
