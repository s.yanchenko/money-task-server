import { Request, Response } from 'express'
import HttpException, { sendApiError } from '@/utils/exceptions/http.exception'
import HttpCode from '@/constants/http.code.constant'
import { registrationValidation } from '@/utils/validations/registration.validation'
import { UserService } from '@/services/user.service'
import { AuthService } from '@/services/auth.service'
import Api from '@/constants/api.constant'

export const AuthController = {
  async registration(req: Request, res: Response) {
    try {
      const { nickname, password } = req.body

      const validateResult = registrationValidation.safeParse({
        nickname,
        password,
      })

      if (!validateResult.success) {
        throw new HttpException(
          HttpCode.BAD_REQUEST,
          JSON.stringify(
            validateResult.error.errors.map((el) => ({
              path: el.path.join('.'),
              message: el.message,
            })),
          ),
        )
      }

      await UserService.create({ nickname, password })

      return res.status(HttpCode.OK).send()
    } catch (error: unknown) {
      sendApiError(error, res)
    }
  },
  async login(req: Request, res: Response) {
    try {
      const { nickname, password } = req.body

      const user = await UserService.get({ nickname })

      const isPassEquals = await AuthService.checkPassword(
        password,
        user.password,
      )

      if (isPassEquals) {
        const accessToken = await AuthService.generateAccessToken(
          String(user._id),
          user.nickname,
          user.roles,
        )

        res.cookie(Api.ACCESS_TOKEN, accessToken, {
          maxAge: 14 * 24 * 60 * 60 * 1000,
          httpOnly: true,
        })
      }

      return res.status(HttpCode.OK).send()
    } catch (error: unknown) {
      sendApiError(error, res)
    }
  },
  async logout(_: Request, res: Response) {
    try {
      res.clearCookie(Api.ACCESS_TOKEN)

      return res.status(HttpCode.OK).send()
    } catch (error: unknown) {
      sendApiError(error, res)
    }
  },
}
