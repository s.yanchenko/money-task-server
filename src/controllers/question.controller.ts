import { Request, Response } from 'express'
import HttpException, { sendApiError } from '@/utils/exceptions/http.exception'
import HttpCode from '@/constants/http.code.constant'
import { QuestionService } from '@/services/question.service'
import Message from '@/constants/message.constant'
import { Answer } from '@/interfaces/question.interface'

export const QuestionController = {
  async getQuestionsByTaskId(req: Request, res: Response) {
    try {
      const { taskId } = req.params

      const questions = await QuestionService.getById({ taskId })

      if (!questions) {
        throw new HttpException(HttpCode.BAD_REQUEST, Message.MISSING_DATA)
      }

      return res.status(HttpCode.OK).send(
        questions.questions.map((el) => ({
          title: el.title,
          variants: el.variants.map((item) => ({
            id: item._id,
            title: item.title,
          })),
        })),
      )
    } catch (error: unknown) {
      sendApiError(error, res)
    }
  },
  async checkQuestionsByTaskId(req: Request, res: Response) {
    try {
      const { taskId } = req.params

      const { answers } = req.body

      const questions = await QuestionService.getById({ taskId })

      if (!questions) {
        throw new HttpException(HttpCode.BAD_REQUEST, Message.MISSING_DATA)
      }

      let rightAnswersCount = 0

      questions.questions.forEach((question) => {
        const findAnswer = answers.find(
          (answer: Answer) => answer.questionId === String(question._id),
        )

        if (question.rightOption === findAnswer?.answerOption) {
          rightAnswersCount++
        }
      })

      return res
        .status(HttpCode.OK)
        .send({ success: rightAnswersCount === questions.questions.length })
    } catch (error: unknown) {
      sendApiError(error, res)
    }
  },
}
