import HttpException, { sendApiError } from '@/utils/exceptions/http.exception'
import { Request, Response } from 'express'
import { AuthService } from '@/services/auth.service'
import { UserService } from '@/services/user.service'
import HttpCode from '@/constants/http.code.constant'
import { AccessTokenData } from '@/interfaces/user.interface'
import Message from '@/constants/message.constant'
import Api from '@/constants/api.constant'
import mongoose from 'mongoose'
import GridFs from 'gridfs-stream'
import dotenv from 'dotenv'
import UserConstant from '@/constants/user.constant'
import { TelegramBotService } from '@/services/telegram-bot.service'

dotenv.config()

export const UserController = {
  async getProfile(req: Request, res: Response) {
    try {
      const { accessToken } = req.cookies

      const tokenData = AuthService.getAccessTokenData(accessToken)

      const user = await UserService.get({
        nickname: (tokenData as AccessTokenData).nickname,
      })

      // TODO: get task data for statistics

      res.status(HttpCode.OK).send({
        nickname: user.nickname,
        avatarUrl: user?.avatarUrl,
        currentBalance: user.currentBalance,
        totalBalance: user.totalBalance,
      })
    } catch (error: unknown) {
      sendApiError(error, res)
    }
  },
  async editProfile(req: Request, res: Response) {
    try {
      const { accessToken } = req.cookies

      const { nickname } = req.body

      const tokenData = AuthService.getAccessTokenData(accessToken)

      await UserService.edit((tokenData as AccessTokenData).id, {
        nickname,
      })

      res.status(HttpCode.OK).send()
    } catch (error: unknown) {
      sendApiError(error, res)
    }
  },
  async editProfileAvatar(req: Request, res: Response) {
    try {
      const { accessToken } = req.cookies

      const filename = req?.file?.filename

      if (!filename) {
        throw new HttpException(HttpCode.BAD_REQUEST, Message.WRONG_DATA)
      }

      const tokenData = AuthService.getAccessTokenData(accessToken)

      const avatarUrl = `${Api.ROOT}:${Api.PORT}${Api.USER}${Api.USER_GET_PROFILE_AVATAR}/${filename}`

      await UserService.edit((tokenData as AccessTokenData).id, {
        avatarUrl,
      })

      res.status(HttpCode.OK).send()
    } catch (error: unknown) {
      sendApiError(error, res)
    }
  },
  async getProfileAvatar(req: Request, res: Response) {
    try {
      const connect = await mongoose.connect(
        process.env.MONGO_URI || Api.MONGO_URI,
      )

      const gridFs = GridFs(connect.connection.db, mongoose.mongo)
      gridFs.collection(Api.BUCKET_NAME)

      const gridFsBucket = new mongoose.mongo.GridFSBucket(
        connect.connection.db,
        {
          bucketName: Api.BUCKET_NAME,
        },
      )

      const file = await gridFs.files.findOne({
        filename: req.params.filename,
      })

      if (!file?.filename) {
        throw new HttpException(HttpCode.NOT_FOUND, Message.MISSING_DATA)
      }

      return gridFsBucket.openDownloadStreamByName(file.filename).pipe(res)
    } catch (error: unknown) {
      sendApiError(error, res)
    }
  },
  async getBalanceData(req: Request, res: Response) {
    try {
      const { accessToken } = req.cookies

      const tokenData = AuthService.getAccessTokenData(accessToken)

      const user = await UserService.get({
        id: (tokenData as AccessTokenData).id,
      })

      res.status(HttpCode.OK).send({
        currentBalance: user.currentBalance,
        availableMoneyForWithdrawal: user.availableMoneyForWithdrawal,
      })
    } catch (error: unknown) {
      sendApiError(error, res)
    }
  },
  async requestWithdrawal(req: Request, res: Response) {
    try {
      const { accessToken } = req.cookies

      const { amount } = req.body

      const tokenData = AuthService.getAccessTokenData(accessToken)

      const user = await UserService.get({
        id: (tokenData as AccessTokenData).id,
      })

      const admin = await UserService.get({
        roles: [UserConstant.ADMIN_ROLE],
      })

      if (!admin || !admin?.chatId || !user) {
        throw new HttpException(HttpCode.NOT_FOUND, Message.MISSING_DATA)
      }

      if (
        amount > user.availableMoneyForWithdrawal ||
        amount > user.currentBalance ||
        user.availableMoneyForWithdrawal === 0
      ) {
        throw new HttpException(
          HttpCode.BAD_REQUEST,
          Message.INVALID_WITHDRAWAL,
        )
      }

      await TelegramBotService.send(
        admin.chatId,
        `Пользователь ${user.nickname} запрашивает вывод средств в количестве ${amount} рублей`,
      )

      await UserService.edit(String(user._id), {
        $inc: { currentBalance: -amount, availableMoneyForWithdrawal: -amount },
      })

      res.status(HttpCode.OK).send()
    } catch (error: unknown) {
      sendApiError(error, res)
    }
  },
}
