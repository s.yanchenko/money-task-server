import { Request, Response } from 'express'
import HttpException, { sendApiError } from '@/utils/exceptions/http.exception'
import HttpCode from '@/constants/http.code.constant'
import Message from '@/constants/message.constant'
import { TaskService } from '@/services/task.service'
import { AuthService } from '@/services/auth.service'
import { AccessTokenData } from '@/interfaces/user.interface'
import { TaskStatus } from '@/constants/task.constant'
import { TaskWithId } from '@/interfaces/task.interface'
import { UserService } from '@/services/user.service'
import notEmpty from '@/utils/filter/not-empty'
import TaskModel from '@/models/task.model'
import { setTaskDisplayedStatus, taskFilter } from '@/utils/filter/task-filter'

export const TaskController = {
  async getTaskById(req: Request, res: Response) {
    try {
      const { taskId } = req.params

      if (!taskId) {
        throw new HttpException(HttpCode.BAD_REQUEST, Message.MISSING_DATA)
      }

      const task = await TaskService.getById({ taskId })

      res.status(HttpCode.OK).send(taskFilter(task))
    } catch (error: unknown) {
      sendApiError(error, res)
    }
  },
  async getExecutionTasks(req: Request, res: Response) {
    try {
      const { accessToken } = req.cookies

      const tokenData = AuthService.getAccessTokenData(accessToken)

      const executionTasks = await TaskService.getByFilter({
        $and: [
          { userId: (tokenData as AccessTokenData).id },
          {
            $or: [
              { status: TaskStatus.PROGRESS },
              { status: TaskStatus.SUSPENDED },
            ],
          },
        ],
      })

      res.status(HttpCode.OK).send(executionTasks.map(taskFilter))
    } catch (error: unknown) {
      sendApiError(error, res)
    }
  },
  async getAvailableTasks(req: Request, res: Response) {
    try {
      const { accessToken } = req.cookies

      const tokenData = AuthService.getAccessTokenData(accessToken)

      const userId: string = (tokenData as AccessTokenData).id

      let resultArray: TaskWithId[] = []

      const user = await UserService.get({
        id: (tokenData as AccessTokenData).id,
      })

      if (!user) {
        throw new HttpException(HttpCode.BAD_REQUEST, Message.MISSING_DATA)
      }

      // Уже показанные задачи
      const displayedTasks = await TaskService.getByFilter({
        $and: [
          { userId },
          {
            $and: [
              { status: TaskStatus.DISPLAYED },
              { taskDisplayedDate: { $exists: true } },
            ],
          },
        ],
      })

      resultArray = displayedTasks.slice(0, user.availableTasksTodayCount)

      if (resultArray.length < user.availableTasksTodayCount) {
        /* Поиск выполненных задач у которых есть связанная задача */
        const completedTasksWithRelatedTaskId = await TaskService.getByFilter({
          $and: [
            { userId },
            {
              $and: [
                { status: TaskStatus.COMPLETED },
                { relatedTaskId: { $exists: true } },
              ],
            },
          ],
        })

        const relatedIds: string[] = completedTasksWithRelatedTaskId
          ?.map((el) => el?.relatedTaskId)
          ?.filter(notEmpty)

        if (relatedIds.length) {
          // Поиск связанных задач
          const resultIds = resultArray.map((el) => el._id)

          const relatedTasks = await TaskService.getByFilter({
            $and: [
              { userId },
              {
                $and: [
                  { _id: { $in: relatedIds } },
                  { _id: { $nin: resultIds } },
                ],
              },
              { status: TaskStatus.AVAILABLE },
            ],
          })

          const slicedRelatedTasks = [...relatedTasks].slice(
            0,
            user.availableTasksTodayCount - resultArray.length,
          )

          resultArray = [...resultArray, ...slicedRelatedTasks].map(
            setTaskDisplayedStatus,
          )

          await TaskModel.updateMany(
            { _id: { $in: slicedRelatedTasks?.map((el) => el?._id) } },
            {
              status: TaskStatus.DISPLAYED,
              taskDisplayedDate: new Date().toISOString(),
            },
          )
        }
      }

      if (resultArray.length < user.availableTasksTodayCount) {
        // Поиск задач, которые доступны для выполнения и ещё не были показаны пользователю
        const availableTasks = await TaskService.getByFilter({
          $and: [
            { userId },
            {
              _id: { $nin: resultArray.map((el) => el._id) },
            },
            {
              status: TaskStatus.AVAILABLE,
            },
          ],
        })

        const slicedAvailableTasks = [...availableTasks].slice(
          0,
          user.availableTasksTodayCount - resultArray.length,
        )

        resultArray = [...resultArray, ...slicedAvailableTasks].map(
          setTaskDisplayedStatus,
        )

        await TaskModel.updateMany(
          { _id: { $in: slicedAvailableTasks?.map((el) => el?._id) } },
          {
            status: TaskStatus.DISPLAYED,
            taskDisplayedDate: new Date().toISOString(),
          },
        )
      }

      res.status(HttpCode.OK).send(resultArray.map(taskFilter))
    } catch (error: unknown) {
      sendApiError(error, res)
    }
  },
  async startTaskById(req: Request, res: Response) {
    try {
      const { taskId } = req.params

      const { accessToken } = req.cookies

      const tokenData = AuthService.getAccessTokenData(accessToken)

      await TaskService.edit(taskId, {
        status: TaskStatus.PROGRESS,
        $push: { dates: { start: new Date().toISOString(), end: null } },
      })

      await UserService.edit((tokenData as AccessTokenData).id, {
        $inc: { availableTasksTodayCount: -1 },
      })

      res.status(HttpCode.OK).send()
    } catch (error: unknown) {
      sendApiError(error, res)
    }
  },
  async toggleTaskTrackerStatusById(req: Request, res: Response) {
    try {
      const { taskId } = req.params

      const { status } = req.body

      if (status === TaskStatus.PROGRESS) {
        await TaskService.edit(
          taskId,
          {
            status: TaskStatus.SUSPENDED,
            $set: { 'dates.$.end': new Date().toISOString() },
          },
          { 'dates.end': null },
        )
      }

      if (status === TaskStatus.SUSPENDED) {
        await TaskService.edit(taskId, {
          status: TaskStatus.PROGRESS,
          $push: { dates: { start: new Date().toISOString(), end: null } },
        })
      }

      res.status(HttpCode.OK).send()
    } catch (error: unknown) {
      sendApiError(error, res)
    }
  },
  async createTask(req: Request, res: Response) {
    try {
      const {
        shortDescription,
        description,
        status,
        type,
        remunerationAmount,
        userId,
      } = req.body

      await TaskService.create(
        {
          shortDescription,
          description,
          status,
          type,
          remunerationAmount,
        },
        userId,
      )

      res.status(HttpCode.OK).send()
    } catch (error: unknown) {
      sendApiError(error, res)
    }
  },
  async createProposedTask(req: Request, res: Response) {
    try {
      const { shortDescription, description, type, remunerationAmount } =
        req.body

      const { accessToken } = req.cookies

      const tokenData = AuthService.getAccessTokenData(accessToken)

      await TaskService.create(
        {
          shortDescription,
          description,
          status: TaskStatus.PROPOSED,
          type,
          remunerationAmount,
        },
        (tokenData as AccessTokenData).id,
      )

      res.status(HttpCode.OK).send()
    } catch (error: unknown) {
      sendApiError(error, res)
    }
  },
  async finishTaskById(req: Request, res: Response) {
    try {
      const { taskId } = req.params

      const { accessToken } = req.cookies

      const tokenData = AuthService.getAccessTokenData(accessToken)

      await TaskService.edit(taskId, {
        status: TaskStatus.COMPLETED,
      })

      const task = await TaskService.getById({ taskId })

      await UserService.edit((tokenData as AccessTokenData).id, {
        $inc: {
          totalBalance: task.remunerationAmount,
          currentBalance: task.remunerationAmount,
        },
      })

      res.status(HttpCode.OK).send()
    } catch (error: unknown) {
      sendApiError(error, res)
    }
  },
}
